tool
extends Node2D

var seen = true
onready var target = position

export var radar_range = 250

func _ready():
	if !Engine.editor_hint:
		if "Enemy" in get_groups():
			seen = false
			target = Vector2(900, 300)

func _physics_process(delta):
	if target:
		if target.distance_to(global_position) <3:
			target = null
		else:
			var diff = target-global_position
			
			diff = diff.clamped(5)
			move_local_x(diff.x)
			move_local_y(diff.y)
	
	if "Enemy" in get_groups():
		seen = false
		for ship in get_parent().get_children():
			if self != ship and "Player" in ship.get_groups():
				if ship.get_script() == get_script():
					if ship.global_position.distance_to(global_position) <= ship.radar_range:
						seen = true
						break
	update()

func _draw():
	if !seen and !Engine.editor_hint:
		return
	
	var inv = get_global_transform().affine_inverse()
	draw_set_transform(inv.get_origin(), inv.get_rotation(), inv.get_scale())
	
	var color = Color(0, 0, 0, 1)
	if "Player" in get_groups():
		color = Color(0, 1, 0, 1)
	
	elif "Enemy" in get_groups():
		if Engine.editor_hint:
			if seen:
				color = Color(1, 0, 0, 1)
			else:
				color = Color(0, 0, 0, 1)
		else:
			color = Color(1, 0, 0, 1)
	
	draw_circle(position, 50, color)
	
	if radar_range > 0:
		if "Player" in get_groups():
			var ships = get_tree().get_nodes_in_group("Player")
			
			# If player inside any other ships radar, don't draw radar arc
			var inside_radar = false
			for ship in ships:
				if ship != self and "Player" in ship.get_groups():
					var dist = ship.global_position.distance_to(global_position)
					if ship.radar_range > radar_range + dist:
						inside_radar = true
						break
		
			if not inside_radar:
				draw_radar_arc(color, ships)

func draw_radar_arc(color, ships):
	var nb_points = max(32, radar_range / 5)
	var points_arc = PoolVector2Array()

	for i in range(nb_points+1):
		var angle_point = deg2rad(i * 360 / nb_points - 90)
		var point = global_position + Vector2(cos(angle_point), sin(angle_point)) * radar_range
		points_arc.push_back(point)
	
	for index_point in range(nb_points):
		var outside = true
		for ship in ships:
			if ship != self and "Player" in ship.get_groups():
				var dist_0 = points_arc[index_point].distance_to(ship.global_position)
				var dist_1 = points_arc[index_point + 1].distance_to(ship.global_position)
				if dist_0 < ship.radar_range:
					if dist_1 < ship.radar_range:
						# Point is inside some other radar, skip drawing
						outside = false
						break
					
		if outside:
			draw_line(points_arc[index_point], points_arc[index_point + 1], color, 1, true)
